package com.example.igri

import android.content.Context
import android.content.SharedPreferences
import android.content.res.Configuration
import android.content.res.Resources
import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.SeekBar
import androidx.appcompat.app.AppCompatDelegate
import androidx.core.app.ActivityCompat.recreate
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation
import com.example.igri.SettingsFolder.LocaleHelper
import com.example.igri.SettingsFolder.SoundsPlayer
import com.example.igri.SettingsFolder.ThemeHelper
import com.example.igri.databinding.FragmentSettingsBinding
import java.util.*

class SettingsFragment : Fragment(R.layout.fragment_settings) {
    private var _binding: FragmentSettingsBinding? = null
    private val binding get() = _binding!!

    private var _settingsPref: SharedPreferences? = null
    private val settingsPref get() = _settingsPref!!

    private var maxVolume = 50


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentSettingsBinding.bind(view)
        _settingsPref = requireActivity().getSharedPreferences("Settings", Context.MODE_PRIVATE)!!
        setListeners()
        initSeekbar()
        initSoundButton()

        with (binding) {
            tvThemeSelectable.text = if (AppCompatDelegate.getDefaultNightMode() == AppCompatDelegate.MODE_NIGHT_YES)
                getString(R.string.theme_dark)
            else getString(R.string.theme_light)
        }
    }

    override fun onDestroyView() {
        _binding = null
        _settingsPref = null
        super.onDestroyView()
    }

    private fun setListeners() {
        with(binding) {
            btnSwithLanguageRight.setOnClickListener {
                changeLanguage(LocaleHelper.getNext())
            }

            btnSwithLanguageLeft.setOnClickListener {
                changeLanguage(LocaleHelper.getPrev())
            }

            btnSwithThemeRight.setOnClickListener {
                changeTheme(ThemeHelper.getNext())
            }

            btnSwithThemeLeft.setOnClickListener {
                changeTheme(ThemeHelper.getPrev())
            }

            btnGoBack.setOnClickListener {
                SoundsPlayer.getPlayer(requireContext(), SoundsPlayer.BTN_SOUND)?.start()
                activity?.onBackPressed()
            }

            btnSound.setOnClickListener {
                changeVolumeStatus()
            }
        }
    }

    private fun changeVolumeStatus() {
        SoundsPlayer.getPlayer(requireContext(), SoundsPlayer.SWITCH_SOUND)?.start()
        if (SoundsPlayer.soundOn)
            binding.btnSound.setImageResource(R.drawable.ic_sounds_off)
        else
            binding.btnSound.setImageResource(R.drawable.ic_sounds_on)
        SoundsPlayer.soundOn = !SoundsPlayer.soundOn
        saveChanges(SOUNDS_KEY, SoundsPlayer.soundOn)
    }

    private fun changeLanguage(language: String) {
        SoundsPlayer.getPlayer(requireContext(), SoundsPlayer.SWITCH_SOUND)?.start()
        setLocale(language, resources)
        saveChanges(LANGUAGE_KEY, language)
        activity?.let { it -> recreate(it) }
    }

    private fun changeTheme(theme: String) {
        SoundsPlayer.getPlayer(requireContext(), SoundsPlayer.SWITCH_SOUND)?.start()
        saveChanges(THEME_KEY, theme)
        setTheme(theme, binding)
    }

    private fun initSeekbar() {
        with(binding) {
            volumeControl.max = maxVolume
            volumeControl.progress = (settingsPref.getFloat(MUSIC_KEY, 1f) * maxVolume).toInt()
            volumeControl.setOnSeekBarChangeListener(object : SeekBar.OnSeekBarChangeListener {
                override fun onProgressChanged(seekBar: SeekBar, progress: Int, fromUser: Boolean) {
                    val value = (1 - ((maxVolume - progress) / maxVolume.toFloat()))
                    saveChanges(MUSIC_KEY, value)
                    (activity as MainActivity).setMediaServiceVolume(value)
                }

                override fun onStartTrackingTouch(p0: SeekBar?) {
                }

                override fun onStopTrackingTouch(p0: SeekBar?) {
                }
            })
        }
    }

    private fun initSoundButton() {
        if (!settingsPref.getBoolean(SOUNDS_KEY, true))
            binding.btnSound.setImageResource(R.drawable.ic_sounds_off)
    }

    private fun saveChanges(key: String, value: String) {
        val editor = settingsPref.edit()
        editor.putString(key, value)
        editor.apply()
    }

    private fun saveChanges(key: String, value: Float) {
        val editor = settingsPref.edit()
        editor.putFloat(key, value)
        editor.apply()
    }

    private fun saveChanges(key: String, value: Boolean) {
        val editor = settingsPref.edit()
        editor.putBoolean(key, value)
        editor.apply()
    }

    companion object {
        val MUSIC_KEY = "saveVolume"
        val SOUNDS_KEY = "saveSoundStatus"
        val LANGUAGE_KEY = "saveLanguage"
        val THEME_KEY = "saveTheme"

        fun setTheme(theme: String, binding: FragmentSettingsBinding? = null) {        //, tv: TextView){ что-то не работает
            if (theme == "light") {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_YES)
            } else {
                AppCompatDelegate.setDefaultNightMode(AppCompatDelegate.MODE_NIGHT_NO)
            }
        }

        fun setLocale(language: String, res: Resources) {
            val locale = Locale(language)
            Locale.setDefault(locale)
            val config = Configuration()
            config.locale = locale
            res.updateConfiguration(config, res.displayMetrics)
        }
    }
}