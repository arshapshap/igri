package com.example.igri

import android.annotation.SuppressLint
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.util.TypedValue
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import androidx.annotation.RequiresApi
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.example.igri.HangmanFolder.HangmanAdapter
import com.example.igri.HangmanFolder.HangmanSymbol
import com.example.igri.HangmanFolder.HangmanSymbolRepository
import com.example.igri.SettingsFolder.SoundsPlayer
import com.example.igri.databinding.FragmentHangmanBinding
import com.example.igri.WordsFolder.WordsRepository.fillRepository
import com.example.igri.WordsFolder.WordsRepository.getLongWord
import com.example.igri.WordsFolder.WordsRepository.longWordsNumber
import java.util.*
import kotlin.collections.ArrayList


class HangmanFragment : Fragment(R.layout.fragment_hangman) {


    private var _binding: FragmentHangmanBinding? = null
    private val binding get() = _binding!!
    private var dialog : DialogBoxFragment? = null
    private var adapter: HangmanAdapter? = null
    private lateinit var symbolsList: ArrayList<HangmanSymbol>
    private var word: String? = null
    private var dictWord: MutableMap<String, ArrayList<Int>> = mutableMapOf()
    private var hangmanParts: ArrayList<ImageView> = ArrayList(10)
    private lateinit var currPlayer: TextView
    private var attempt: Int = 0


    @RequiresApi(Build.VERSION_CODES.Q)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentHangmanBinding.bind(view)
        dialog = DialogBoxFragment(context, view)

        initializeFragment()

        logInfo()

        val trueAnswerColor = requireContext().let { ContextCompat.getColor(it,R.color.true_answer) }
        val falseAnswerColor = requireContext().let { ContextCompat.getColor(it,R.color.false_answer) }
        val currentPrimaryColor = getPrimaryColor()

        symbolsList = HangmanSymbolRepository.returnDefaultList(currentPrimaryColor, requireContext())

        adapter = HangmanAdapter(symbolsList) {
            keyBoardLogic(it, trueAnswerColor, falseAnswerColor)
        }
        binding.rvHangmanAlphabet.adapter = adapter
    }

    //Получение цвета из текущей темы
    @RequiresApi(Build.VERSION_CODES.Q)
    private fun getPrimaryColor(): Int {
        val a = TypedValue()
        requireActivity().theme?.resolveAttribute(android.R.attr.colorPrimary, a, true)
        return if (a.isColorType) a.data else 0
    }

    private fun keyBoardLogic(
        symbol: HangmanSymbol,
        trueAnswerColor: Int,
        falseAnswerColor: Int
    ) {
        SoundsPlayer.getPlayer(requireContext(), SoundsPlayer.BTN_SOUND)?.start()
        if (symbol.isClickable) {
            if (dictWord.containsKey(symbol.symbol)) {
                symbol.color = trueAnswerColor
                updateText(symbol)
            } else if (attempt >= hangmanParts.size - 1) {
                endGame(symbol, falseAnswerColor)
            } else {
                symbol.color = falseAnswerColor
                hangmanParts[attempt++].visibility = View.VISIBLE
            }
            symbol.isClickable = false
            changePlayer()
            binding.rvHangmanAlphabet.adapter?.notifyItemChanged(symbol.id)
        }
    }

    private fun updateText(symbol: HangmanSymbol) {
        val symbols: ArrayList<Char> = ArrayList()
        for (i in 0 until word!!.length) {
            if (!dictWord[symbol.symbol]!!.contains(i))
                symbols.add(binding.tvHangmanWord.text[i])
            else {
                currPlayer.text = (currPlayer.text.toString().toInt().inc()).toString()
                symbols.add(symbol.symbol.first())
            }
        }
        binding.tvHangmanWord.text = String(symbols.toCharArray())
        if (binding.tvHangmanWord.text.all { c -> c != '_' })
            updateGame()
    }

    private fun initializeFragment() {
        fillRepository(resources.getStringArray(R.array.words))
        word = getLongWord(Random(System.currentTimeMillis()).nextInt(longWordsNumber())).uppercase()
        word!!.forEachIndexed { index, c ->
            run {
                if (dictWord.containsKey(c.toString())) dictWord[c.toString()]!!.add(index)
                else {
                    dictWord[c.toString()] = ArrayList()
                    dictWord[c.toString()]!!.add(index)
                }
            }
        }
        with(binding) {
            tvHangmanWord.text = String(word!!.map { '_' }.toCharArray())
            hangmanParts.add(ivHangmanStrut)
            hangmanParts.add(ivHangmanWoodenStand)
            hangmanParts.add(ivHangmanCrossbar)
            hangmanParts.add(ivHangmanRope)
            hangmanParts.add(ivHangmanHead)
            hangmanParts.add(ivHangmanBody)
            hangmanParts.add(ivHangmanRightLeg)
            hangmanParts.add(ivHangmanLeftLeg)
            hangmanParts.add(ivHangmanRightArm)
            hangmanParts.add(ivHangmanLeftArm)
            hangmanParts.forEach { it.visibility = View.INVISIBLE }

            ivCurrP1.visibility = View.VISIBLE
            ivCurrP2.visibility = View.INVISIBLE
            tvP1Score.text = "0"
            tvP2Score.text = "0"
            currPlayer = tvP1Score

            btnHome.setOnClickListener {
                SoundsPlayer.getPlayer(requireContext(), SoundsPlayer.BTN_SOUND)?.start()
                activity?.onBackPressed()
            }

        }
    }

    private fun changePlayer(){
        if(currPlayer == binding.tvP1Score) {
            currPlayer = binding.tvP2Score
            binding.ivCurrP1.visibility = View.INVISIBLE
            binding.ivCurrP2.visibility = View.VISIBLE
        }
        else{
            currPlayer = binding.tvP1Score
            binding.ivCurrP1.visibility = View.VISIBLE
            binding.ivCurrP2.visibility = View.INVISIBLE
        }
    }

    @SuppressLint("NotifyDataSetChanged")
    private fun endGame(symbol: HangmanSymbol, falseAnswerColor: Int) {
        symbol.color = falseAnswerColor
        hangmanParts[attempt].visibility = View.VISIBLE
        for(symbols in symbolsList)
            symbols.isClickable = false
        binding.rvHangmanAlphabet.adapter?.notifyDataSetChanged()
        updateGame()
    }

    private fun updateGame(){
        dialog?.showDialog(getWinner(), R.id.action_hangmanFragment_self, R.id.action_hangmanFragment_to_mainFragemnt)
    }

    private fun getWinner(): String {
        val playerOneScore: Int = binding.tvP1Score.text.toString().toInt()
        val playerTwoScore: Int = binding.tvP2Score.text.toString().toInt()
        return if (playerOneScore > playerTwoScore) "${getString(R.string.player)} 1 ${getString(R.string.winner)}"
        else if (playerOneScore < playerTwoScore) "${getString(R.string.player)} 2 ${getString(R.string.winner)}"
        else getString(R.string.draw)
    }

    private fun logInfo() {
        Log.e("keys", dictWord.keys.toString())
        Log.e("values", dictWord.values.toString())
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
    }
}