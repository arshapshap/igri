package com.example.igri

import android.content.Context
import android.os.Bundle
import android.text.*
import android.text.InputFilter.AllCaps
import android.view.*
import android.view.inputmethod.InputMethodManager
import android.widget.EditText
import android.widget.ScrollView
import android.widget.TableRow
import androidx.core.widget.doAfterTextChanged
import androidx.fragment.app.Fragment
import com.example.igri.SettingsFolder.SoundsPlayer
import com.example.igri.WordsFolder.WordsRepository.fillRepository
import com.example.igri.WordsFolder.WordsRepository.isWord
import com.example.igri.databinding.FragmentTroikaBinding
import java.lang.Math.abs
import java.lang.Math.max


class TroikaFragment : Fragment(R.layout.fragment_troika) {
    private var _binding: FragmentTroikaBinding? = null
    private val binding get() = _binding!!

    private var isFirstPlayerTurn = true
    private var isMoveOver = false
    private var selectedCells = arrayListOf<EditText>()
    private var score = arrayOf(0, 0)
    private var wordsHistory = arrayListOf<String>()
    private var wrongWordsHistory = arrayListOf<String>()
    private var coordinates = mutableMapOf<EditText, Pair<Int, Int>>()
    private var lettersCount = 0
    private var emptyCells = arrayListOf<EditText>()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentTroikaBinding.bind(view)

        // sharedPref больше не нужен, но удалять всё рука не поднимается
        // loadPreferences()

        parseGrid()
        output("${getString(R.string.player)} ${getCurrentPlayer() + 1}, ${getString(R.string.enter_letter)}")
        fillRepository(resources.getStringArray(R.array.words))
        binding.btnHome.setOnClickListener {
            SoundsPlayer.getPlayer(requireContext(), SoundsPlayer.BTN_SOUND)?.start()
            activity?.onBackPressed()
        }

        changeCompleteButtonVisibility(false)
        binding.btnComplete.setOnClickListener { completeMove() }
    }

    override fun onDestroyView() {
        _binding = null
        super.onDestroyView()
        /*
        val preferences = activity?.getSharedPreferences("TROIKA", Context.MODE_PRIVATE)!!
        preferences.edit {
            clear()
            apply()
        }*/
    }

    private fun parseGrid() {
        for (i in 0..2) {
            for (k in 0..2) {
                val cell = (binding.tlGrid.getChildAt(i) as TableRow).getChildAt(k) as EditText
                cell.filters = arrayOf(
                    AllCaps(),
                    InputFilter.LengthFilter(1),
                    InputFilterOnlyLetters(resources.getString(R.string.alphabet))
                )
                cell.inputType = InputType.TYPE_TEXT_FLAG_NO_SUGGESTIONS
                cell.isLongClickable = false
                cell.doAfterTextChanged { addLetter(cell) }
                emptyCells.add(cell)
                coordinates[cell] = Pair(i, k)
            }
        }
    }

    private fun loadPreferences() {
        val preferences = activity?.getSharedPreferences("TROIKA", Context.MODE_PRIVATE)!!

        val wordsString = preferences.getString("WORDS_HISTORY", "")!!
        if (wordsString.isNotEmpty())
            wordsHistory = ArrayList(wordsString.split(" "))

        val wrongWordsString = preferences.getString("WRONG_WORDS_HISTORY", "")!!
        if (wrongWordsString.isNotEmpty())
            wrongWordsHistory = ArrayList(wrongWordsString.split(" "))

        binding.tvWords.text = wordsHistory.joinToString("\n")
        binding.tvP1Score.text = "P1: ${preferences.getInt("SCORE_P1", 0)}"
        binding.tvP2Score.text = "P2: ${preferences.getInt("SCORE_P2", 0)}"
    }

    private fun getPlayerColor(): Int {
        if (isFirstPlayerTurn)
            return resources.getColor(R.color.primary, context?.theme)
        return resources.getColor(R.color.secondary, context?.theme)
    }

    private fun addLetter(cell: EditText) {
        if (cell.text.isEmpty())
            return

        isMoveOver = false
        closeKeyboard(cell)
        clearSelectedCells()
        markReachableCells()

        emptyCells.forEach { it.isEnabled = false }
        emptyCells.remove(cell)

        cell.setTextColor(getPlayerColor())
        cell.setOnClickListener { trySelect(it as EditText) }
        cell.isFocusable = false
        cell.isEnabled = true

        lettersCount++
        if (lettersCount < 3) {
            completeMove()
        } else {
            output("${getString(R.string.player)} ${getCurrentPlayer() + 1}, ${getString(R.string.find_word)}")
            changeCompleteButtonVisibility(true)
        }
    }

    private fun trySelect(cell: EditText) {
        if (selectedCells.contains(cell)) {
            unselectCell(cell)
            selectedCells.remove(cell)
            markReachableCells()
            return
        }
        if (!isCellReachable(cell) || isMoveOver)
            return


        selectedCells.add(cell)
        selectCell(cell)

        if (selectedCells.size == 3)
            addWord(getWord())

        markReachableCells()
    }

    private fun completeMove(errorWord: String = "") {
        isMoveOver = true
        changeCompleteButtonVisibility(false)

        if (lettersCount == 9 && getCurrentPlayer() == 0) {
            changeCurrentPlayer()
            output("${getString(R.string.player)} ${getCurrentPlayer() + 1}, ${getString(R.string.find_word)}")
            changeCompleteButtonVisibility(true)
            isMoveOver = false
            return
        }
        else if (lettersCount == 9 && getCurrentPlayer() == 1)
        {
            gameOver()
            return
        }

        changeCurrentPlayer()
        var message =
            "${getString(R.string.player)} ${getCurrentPlayer() + 1}, ${getString(R.string.enter_letter)}"
        if (errorWord.isNotEmpty())
            message =
                "${getString(R.string.word)} \"${errorWord}\" ${getString(R.string.nonexistent)}\n" + message
        output(message)

        emptyCells.forEach { it.isEnabled = true }
    }

    private fun changeCompleteButtonVisibility(visibility: Boolean) {
        if (visibility) {
            binding.btnComplete.isEnabled = true
            binding.btnComplete.visibility = View.VISIBLE
            binding.tvComplete.isEnabled = true
            binding.tvComplete.visibility = View.VISIBLE
        } else {
            binding.btnComplete.isEnabled = false
            binding.btnComplete.visibility = View.INVISIBLE
            binding.tvComplete.isEnabled = false
            binding.tvComplete.visibility = View.INVISIBLE
        }
    }

    private fun changeCurrentPlayer() {
        isFirstPlayerTurn = !isFirstPlayerTurn
        if (isFirstPlayerTurn) {
            binding.ivCurrP1.visibility = View.VISIBLE
            binding.ivCurrP2.visibility = View.INVISIBLE
        } else {
            binding.ivCurrP1.visibility = View.INVISIBLE
            binding.ivCurrP2.visibility = View.VISIBLE
        }
    }

    private fun gameOver() {
        val message = if (score[0] > score[1])
            "${getString(R.string.player)} 1 ${getString(R.string.winner)}"
        else if (score[0] < score[1])
            "${getString(R.string.player)} 2 ${getString(R.string.winner)}"
        else
            getString(R.string.draw)
        output(message)

        DialogBoxFragment(context, requireView()).showDialog(
            message,
            R.id.action_troikaFragment_self,
            R.id.action_troikaFragment_to_mainFragemnt
        )
    }

    private fun getWord(): String {
        var word = ""
        selectedCells.forEach { word += it.text }
        return word
    }

    private fun markReachableCells() {
        coordinates.keys.forEach {
            it.alpha = if (!isCellReachable(it) && !selectedCells.contains(it)) 0.25f else 1f
        }
    }

    private fun isCellReachable(cell: EditText): Boolean {
        if (selectedCells.isEmpty())
            return true
        if (isWordInHistory(getWord() + cell.text.toString()))
            return false

        val (lastX, lastY) = coordinates[selectedCells.last()]!!
        val (newX, newY) = coordinates[cell]!!

        return max(abs(lastX - newX), abs(lastY - newY)) == 1
    }

    private fun addWord(word: String) {
        if (checkWord(word)) {
            SoundsPlayer.getPlayer(requireContext(), SoundsPlayer.ANSWER_SOUND)?.start()
            addScore()
            /* тут был частично цветной текст, но видимо он не пригодится(
            var text = "${binding.tvWords.text}\n$word"
            var spannable = SpannableString(text)
            spannable.setSpan(ForegroundColorSpan(getPlayerColor()), text.length-4, text.length, Spannable.SPAN_EXCLUSIVE_EXCLUSIVE)
            binding.tvWords.setText(spannable, TextView.BufferType.SPANNABLE)
            */

            wordsHistory.add(word)
            binding.tvWords.text = "${binding.tvWords.text}$word\n"
            binding.svWords.fullScroll(ScrollView.FOCUS_DOWN)

            /* val preferences = activity?.getSharedPreferences("TROIKA", Context.MODE_PRIVATE)
            preferences!!.edit { putString("WORDS_HISTORY", wordsHistory.joinToString(" ")) } */
        } else {
            wrongWordsHistory.add(word)

            completeMove(word)

            /*
            val preferences = activity?.getSharedPreferences("TROIKA", Context.MODE_PRIVATE)
            preferences!!.edit { putString("WRONG_WORDS_HISTORY", wrongWordsHistory.joinToString(" ")) }
             */
        }

        clearSelectedCells()
    }

    private fun output(text: String) {
        binding.tvOutput.text = text
    }

    private fun selectCell(cell: EditText) {
        cell.scaleX = 1.5f
        cell.scaleY = 1.5f
    }

    private fun unselectCell(cell: EditText) {
        cell.scaleX = 1f
        cell.scaleY = 1f
    }

    private fun clearSelectedCells() {
        selectedCells.forEach { unselectCell(it) }
        selectedCells = arrayListOf()
    }

    private fun checkWord(word: String) =
        (!isWordInHistory(word) && isWord(word.lowercase()))

    private fun isWordInHistory(word: String) =
        (wordsHistory.contains(word) || wrongWordsHistory.contains(word))

    private fun addScore() {
        score[getCurrentPlayer()]++
        if (isFirstPlayerTurn)
            binding.tvP1Score.text = "P1: ${score[0]}"
        else
            binding.tvP2Score.text = "P2: ${score[1]}"

        /*
        val pref = activity?.getSharedPreferences("TROIKA", Context.MODE_PRIVATE)

        pref!!.edit {
            putInt("SCORE_P1", score[0])
            putInt("SCORE_P2", score[1])
        }*/
    }

    private fun closeKeyboard(view: View) {
        val inputMethodManager =
            context?.getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        inputMethodManager.hideSoftInputFromWindow(view.windowToken, 0)
    }

    private fun getCurrentPlayer() = if (isFirstPlayerTurn) 0 else 1

    private class InputFilterOnlyLetters(val alphabet: String) : InputFilter {
        override fun filter(
            source: CharSequence?,
            start: Int,
            end: Int,
            dest: Spanned?,
            dstart: Int,
            dend: Int
        ): CharSequence {
            var filtered: String? = ""
            for (i in start..end) {
                if (source == null || i >= source.length)
                    break
                val char = source[i]
                if (alphabet.contains(char, true)) {
                    filtered += char
                }
            }

            return filtered!!
        }
    }
}