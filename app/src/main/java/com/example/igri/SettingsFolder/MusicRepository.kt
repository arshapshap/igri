package com.example.igri.SettingsFolder

import com.example.igri.R
import java.util.Collections.shuffle

class MusicRepository {
    private val musicLibrary = listOf(
        R.raw.music_01,
        R.raw.music_02,
        R.raw.music_03,
        R.raw.music_04,
        R.raw.music_05,
        R.raw.music_06
    )
    private var currentTrack = 0

    fun getNextTrack(): Int {
        var result: Int
        if (currentTrack == 0) {
            shuffle(musicLibrary)
            result = musicLibrary[currentTrack++]
        } else if (currentTrack == musicLibrary.size - 1) {
            result = musicLibrary[currentTrack]
            currentTrack = 0
        } else {
            result = musicLibrary[currentTrack++]
        }
        return result
    }
}