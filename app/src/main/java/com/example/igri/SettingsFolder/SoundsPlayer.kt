package com.example.igri.SettingsFolder

import android.content.Context
import android.media.MediaPlayer
import com.example.igri.R
import java.lang.Exception

object SoundsPlayer {
    var BTN_SOUND = R.raw.btn_interface
    var SWITCH_SOUND = R.raw.btn_soft
    var ANSWER_SOUND = R.raw.btn_interface

    var soundOn = true

    fun getPlayer(context: Context, sound: Int): MediaPlayer? {
        if (!soundOn) return null
        try {
            val mp = MediaPlayer.create(context, sound)
            return mp
        } catch (e: Exception) {
            return null
        }
    }
}