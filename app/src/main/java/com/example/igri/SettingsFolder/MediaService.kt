package com.example.igri.SettingsFolder

import android.app.Service
import android.content.Intent
import android.media.MediaPlayer

import android.os.IBinder


class MediaService : Service() {
    private lateinit var musicMediaPlayer: MediaPlayer
    private val musicRepository = MusicRepository()

    override fun onBind(p0: Intent?): IBinder? {
        return null
    }

    override fun onCreate() {
        super.onCreate()
        musicMediaPlayer = MediaPlayer.create(this, musicRepository.getNextTrack())
        musicMediaPlayer.setOnCompletionListener {
            changeTrack()
        }
    }

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        when (intent?.getAction()) {
            PLAY_ACTION -> musicMediaPlayer.start();
            STOP_ACTION -> if (musicMediaPlayer != null) musicMediaPlayer.pause();
            CHANGE_VOLUME_ACTION -> {
                val value = intent.getFloatExtra(VOLUME_KEY, 50f)
                musicMediaPlayer.setVolume(value, value)
            }
            else -> throw Exception()
        }
        return START_STICKY
    }

    override fun onDestroy() {
        super.onDestroy()
        musicMediaPlayer.stop()
    }

    private fun changeTrack() {
        musicMediaPlayer.reset()
        var afd = resources.openRawResourceFd(musicRepository.getNextTrack())
        musicMediaPlayer.setDataSource(afd.fileDescriptor, afd.startOffset, afd.length)
        musicMediaPlayer.prepare()
        musicMediaPlayer.start()
    }


    companion object {
        val PLAY_ACTION = "com.app.action.play"
        val STOP_ACTION = "com.app.action.stop"
        val CHANGE_VOLUME_ACTION = "com.app.action.changeVolume"

        val VOLUME_KEY = "volume"
    }
}