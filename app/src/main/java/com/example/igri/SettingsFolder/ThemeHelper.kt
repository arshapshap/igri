package com.example.igri.SettingsFolder

import androidx.appcompat.app.AppCompatDelegate

object ThemeHelper {
    private var themes = listOf("light", "dark")
    private var current = 0

    fun getNext(): String {
        current++
        if (current >= themes.size)
            current = 0
        return themes[current]
    }

    fun getPrev(): String {
        current--
        if (current < 0)
            current = themes.size - 1
        return themes[current]
    }

    fun restoreCurrent(theme: String){
        current = themes.indexOf(theme)
    }
}