package com.example.igri.SettingsFolder

import androidx.appcompat.app.AppCompatDelegate

object LocaleHelper {
    private var languages = listOf("en", "ru")
    private var current = 0

    fun getNext(): String {
        current++
        if (current >= languages.size)
            current = 0
        return languages[current]
    }

    fun getPrev(): String {
        current--
        if (current < 0)
            current = languages.size - 1
        return languages[current]
    }

    fun restoreCurrent(language: String){
        current = languages.indexOf(language)
    }
}