package com.example.igri

import android.os.Bundle
import android.os.CountDownTimer
import android.text.Editable
import android.text.TextWatcher
import android.util.Log
import android.view.View
import android.widget.ScrollView
import androidx.fragment.app.Fragment
import com.example.igri.SettingsFolder.SoundsPlayer
import com.example.igri.WordsFolder.WordsRepository.fillRepository
import com.example.igri.WordsFolder.WordsRepository.getLongWord
import com.example.igri.WordsFolder.WordsRepository.isSubword
import com.example.igri.WordsFolder.WordsRepository.isWord
import com.example.igri.WordsFolder.WordsRepository.longWordsNumber
import com.example.igri.databinding.FragmentWordsFromWordBinding
import kotlin.Exception
import kotlin.random.Random

class WordsFragment : Fragment(R.layout.fragment_words_from_word) {
    private var _binding: FragmentWordsFromWordBinding? = null
    private val binding get() = _binding!!


    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        _binding = FragmentWordsFromWordBinding.bind(view)
        binding.tvTimer.text = getString(R.string.loading)
        fillRepository(resources.getStringArray(R.array.words))
        val mainWord = getLongWord(Random(System.currentTimeMillis()).nextInt(longWordsNumber()))
        val guessedWords: ArrayList<String> = arrayListOf()
        var isFirstPlayerMove = false
        var isSecondPlayerMove = false
        var p1points = 0
        var p2points = 0
        with(binding) {
            tvMainWord.text = ""
            ivCurrP1.visibility = View.INVISIBLE
            tvWordsP1Score.text = "P1: $p1points"
            svWords.fullScroll(ScrollView.FOCUS_DOWN)
            tvWordsP2Score.text = "P2: $p2points"
            wordEntry.addTextChangedListener(object : TextWatcher {
                override fun afterTextChanged(s: Editable?) {
                    val entry = s.toString()
                    if (entry.length >= 3 && isWord(entry) && !guessedWords.contains(entry)
                        && isSubword(mainWord, entry) && (isFirstPlayerMove || isSecondPlayerMove)
                    ) {
                        tvWords.text = "${binding.tvWords.text}$entry\n"
                        guessedWords.add(entry)
                        wordEntry.setText("")
                        if (isFirstPlayerMove) {
                            SoundsPlayer.getPlayer(requireContext(), SoundsPlayer.ANSWER_SOUND)
                                ?.start()
                            p1points += 1
                            tvWordsP1Score.text = "P1: $p1points"
                        }
                        if (isSecondPlayerMove) {
                            SoundsPlayer.getPlayer(requireContext(), SoundsPlayer.ANSWER_SOUND)
                                ?.start()
                            p2points += 1
                            tvWordsP2Score.text = "P2: $p2points"
                        }
                        svWords.fullScroll(ScrollView.FOCUS_DOWN)
                    }
                }

                override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                }

                override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {
                }
            })
            btnHome.setOnClickListener {
                SoundsPlayer.getPlayer(requireContext(), SoundsPlayer.BTN_SOUND)?.start()
                activity?.onBackPressed()
            }
        }

        val secondPlayerMoveTimer = object : CountDownTimer(60000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                try {
                    binding.tvTimer.text =
                        "${getString(R.string.time_remained)}: ${millisUntilFinished / 1000} ${
                            getString(R.string.seconds)
                        }"
                } catch (e: Exception) {
                    cancel()
                }
            }

            override fun onFinish() {
                binding.tvWords.text = ""
                isSecondPlayerMove = false
                binding.ivCurrP2.visibility = View.INVISIBLE
                DialogBoxFragment(context, view).showDialog(
                    getWinner(),
                    R.id.action_wordsFragment_self,
                    R.id.action_wordsFragment_to_mainFragemnt
                )
            }

            private fun getWinner(): String {
                return if (p1points > p2points) "${getString(R.string.player)} 1 ${getString(R.string.winner)}"
                else if (p1points < p2points) "${getString(R.string.player)} 2 ${getString(R.string.winner)}"
                else getString(R.string.draw)
            }
        }
        val preSecondPlayerMoveTimer = object : CountDownTimer(15000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                try {
                    binding.tvTimer.text =
                        "${getString(R.string.prepare_to_play)}. \n"
                    binding.tvMainWord.text = "${millisUntilFinished / 1000} ${
                        getString(
                            R.string.seconds
                        )
                    }"
                } catch (e: Exception) {
                    cancel()
                }

            }

            override fun onFinish() {
                binding.tvWords.text = ""
                binding.tvMainWord.text = mainWord
                isSecondPlayerMove = true
                binding.ivCurrP2.visibility = View.VISIBLE
                secondPlayerMoveTimer.start()
            }
        }

        val firstPlayerMoveTimer = object : CountDownTimer(60000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                try {
                    binding.tvTimer.text =
                        "${getString(R.string.time_remained)}: ${millisUntilFinished / 1000} ${
                            getString(R.string.seconds)
                        }"
                } catch (e: Exception) {
                    cancel()
                }
            }

            override fun onFinish() {
                binding.tvWords.text = ""
                binding.tvMainWord.text = ""
                isFirstPlayerMove = false
                binding.ivCurrP1.visibility = View.INVISIBLE
                binding.wordEntry.setText("")
                guessedWords.clear()
                preSecondPlayerMoveTimer.start()
            }
        }

        val preFirstPlayerMoveTimer = object : CountDownTimer(3000, 1000) {
            override fun onTick(millisUntilFinished: Long) {
                try {
                    binding.tvTimer.text =
                        "${getString(R.string.prepare_to_play)}. \n"
                    binding.tvMainWord.text = "${millisUntilFinished / 1000} ${
                        getString(
                            R.string.seconds
                        )
                    }"
                } catch (e: Exception) {
                    cancel()
                }
            }

            override fun onFinish() {
                binding.tvWords.text = ""
                binding.tvMainWord.text = mainWord
                isFirstPlayerMove = true
                binding.ivCurrP1.visibility = View.VISIBLE
                firstPlayerMoveTimer.start()
            }
        }
        preFirstPlayerMoveTimer.start()
    }
}