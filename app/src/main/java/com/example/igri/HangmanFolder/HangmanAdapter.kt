package com.example.igri.HangmanFolder

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.example.igri.databinding.ItemSymbolBinding

class HangmanAdapter(
    private val list: List<HangmanSymbol>,
    private val onItemClick: (HangmanSymbol) -> Unit
): RecyclerView.Adapter<HangmanHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HangmanHolder = HangmanHolder(
        binding = ItemSymbolBinding.inflate(LayoutInflater.from(parent.context), parent, false),
        onItemClick = onItemClick
    )

    override fun onBindViewHolder(holder: HangmanHolder, position: Int) {
        holder.onBind(list[position])
    }

    override fun getItemCount(): Int = list.size
}