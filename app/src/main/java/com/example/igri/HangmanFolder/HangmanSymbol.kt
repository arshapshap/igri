package com.example.igri.HangmanFolder


data class HangmanSymbol(
    val id: Int,
    val symbol: String,
    var color: Int,
    var isClickable: Boolean
)
