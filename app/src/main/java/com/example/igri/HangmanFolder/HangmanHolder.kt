package com.example.igri.HangmanFolder

import androidx.recyclerview.widget.RecyclerView
import com.example.igri.databinding.ItemSymbolBinding

class HangmanHolder(
    private val binding: ItemSymbolBinding,
    private val onItemClick: (HangmanSymbol) -> Unit
): RecyclerView.ViewHolder(binding.root) {

    fun onBind(hangmanSymbol: HangmanSymbol){
        with(binding){
            tvHangmanSymbol.text = hangmanSymbol.symbol
            tvHangmanSymbol.setTextColor(hangmanSymbol.color)
            root.isClickable = hangmanSymbol.isClickable
            root.setOnClickListener{
                onItemClick(hangmanSymbol)
            }
        }
    }
}