package com.example.igri.HangmanFolder

import android.content.Context
import android.graphics.Color
import com.example.igri.R

object HangmanSymbolRepository {

    fun returnDefaultList(color: Int, context: Context): ArrayList<HangmanSymbol> {
        var symbols = ArrayList<HangmanSymbol>((context.getString(R.string.alphabet).mapIndexed { index, c ->
            HangmanSymbol(
                index,
                c.uppercase(),
                color,
                true
            )
        }).toList())
        val lastIndex = symbols.size
        symbols.add(HangmanSymbol(lastIndex, "-", color, true))
        return symbols
    }
}