package com.example.igri

import android.content.Intent
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import com.example.igri.SettingsFolder.LocaleHelper
import com.example.igri.SettingsFolder.MediaService
import com.example.igri.SettingsFolder.SoundsPlayer
import com.example.igri.SettingsFolder.ThemeHelper


class MainActivity : AppCompatActivity() {

    private lateinit var controller: NavController

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        controller =
            (supportFragmentManager.findFragmentById(R.id.container) as NavHostFragment).navController

        startMediaService(MediaService.PLAY_ACTION)
        restoreSettings()
    }

    override fun onPause() {
        startMediaService(MediaService.STOP_ACTION)
        super.onPause()
    }

    override fun onResume() {
        startMediaService(MediaService.PLAY_ACTION)
        super.onResume()
    }

    private fun restoreSettings() {
        val pref = getSharedPreferences("Settings", MODE_PRIVATE)

        val musicVolume = pref.getFloat(SettingsFragment.MUSIC_KEY, 1f)
        setMediaServiceVolume(musicVolume)

        SoundsPlayer.soundOn = pref.getBoolean(SettingsFragment.SOUNDS_KEY, true)

        val theme = pref.getString(SettingsFragment.THEME_KEY, "light")
        SettingsFragment.setTheme(theme!!)
        ThemeHelper.restoreCurrent(theme)

        val language = pref.getString(SettingsFragment.LANGUAGE_KEY, "en")
        SettingsFragment.setLocale(language!!, resources)
        LocaleHelper.restoreCurrent(language)
    }

    private fun startMediaService(action: String, volume: Float? = null) {
        var serviceIntent = Intent(this, MediaService::class.java)
        serviceIntent.setAction(action)
        if (volume != null) serviceIntent.putExtra(MediaService.VOLUME_KEY, volume)
        startService(serviceIntent)
    }

    fun setMediaServiceVolume(volume: Float) {
        startMediaService(MediaService.CHANGE_VOLUME_ACTION, volume)
    }
}
