package com.example.igri

import android.os.Bundle
import android.provider.UserDictionary
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation.findNavController
import com.example.igri.SettingsFolder.SoundsPlayer
import com.example.igri.databinding.FragmentMainBinding

data class Game(
    val title: Int,
    val description: Int,
    val icon: Int
)

class MainFragment : Fragment(R.layout.fragment_main) {
    private var _binding: FragmentMainBinding? = null
    private val binding get() = _binding!!

    private val gameList = listOf(
        Game(
            R.string.game_name_troika,
            R.string.game_description_troika,
            R.drawable.ic_menu_troika
        ),
        Game(
            R.string.game_name_words,
            R.string.game_description_words,
            R.drawable.ic_menu_words
        ),
        Game(
            R.string.game_name_hangman,
            R.string.game_description_hangman,
            R.drawable.ic_menu_hangman
        )
    )

    private var currentPage: Int = 0

    override fun onResume() {
        super.onResume()
        if (currentPage != 0) {
            binding.btnLeft.setImageResource(R.drawable.ic_menu_select_left)
            setGamePreview(gameList[currentPage])
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        _binding = FragmentMainBinding.bind(view)
        setListeners(view)
    }

    private fun setListeners(view: View) {
        with(binding) {
            btnSettings.setOnClickListener {
                SoundsPlayer.getPlayer(requireContext(), SoundsPlayer.BTN_SOUND)?.start()
                findNavController(view).navigate(
                    R.id.action_mainFragemnt_to_settingsFragment,
                    Bundle()
                )
            }

            btnLeft.setOnClickListener {
                SoundsPlayer.getPlayer(requireContext(), SoundsPlayer.SWITCH_SOUND)?.start()
                onLeftClick()
            }

            btnRight.setOnClickListener {
                SoundsPlayer.getPlayer(requireContext(), SoundsPlayer.SWITCH_SOUND)?.start()
                onRightClick()
            }

            btnPlay.setOnClickListener {
                SoundsPlayer.getPlayer(requireContext(), SoundsPlayer.BTN_SOUND)?.start()
                startGame(view)
            }
        }
    }

    private fun startGame(view: View) {
        when (currentPage) {
            0 -> findNavController(view).navigate(
                R.id.action_mainFragemnt_to_troikaFragment,
                Bundle()
            )
            1 -> findNavController(view).navigate(
                R.id.action_mainFragemnt_to_wordsFragment,
                Bundle()
            )
            2 -> findNavController(view).navigate(
                R.id.action_mainFragemnt_to_hangmanFragment,
                Bundle()
            )
        }
    }

    private fun onLeftClick() {
        if (currentPage == 0) return
        binding.btnRight.setImageResource(R.drawable.ic_menu_select_right)
        currentPage--
        setGamePreview(gameList[currentPage])
    }

    private fun onRightClick() {
        if (currentPage == gameList.size - 1) return
        binding.btnLeft.setImageResource(R.drawable.ic_menu_select_left)
        currentPage++
        setGamePreview(gameList[currentPage])
    }

    private fun setGamePreview(game: Game) {
        if (currentPage == 0) binding.btnLeft.setImageResource(R.drawable.ic_menu_select_left_unavailable)
        else if (currentPage == gameList.size - 1) binding.btnRight.setImageResource(R.drawable.ic_menu_select_right_unavailable)
        with(binding) {
            tvGameTitle.text = getString(game.title)
            tvGameDescription.text = getString(game.description)
            previewIcon.setImageResource(game.icon)
            tvPageCounter.text = (currentPage + 1).toString()
        }
    }
}