package com.example.igri

import android.app.AlertDialog
import android.app.Dialog
import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.util.Log
import android.view.View
import android.widget.Button
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.navigation.Navigation.findNavController
import com.example.igri.SettingsFolder.SoundsPlayer
import com.google.android.material.snackbar.Snackbar


class DialogBoxFragment(private val fragmentContext: Context?, private val view: View) {

    private var dialog: Dialog? = null

    fun showDialog(text: String, restartAction: Int, menuAction: Int) {
        if(dialog != null && dialog!!.isShowing) {
            Log.e("${fragmentContext?.javaClass?.simpleName}", "no more crashes")
            return
        }
        fragmentContext?.let { fragment ->
            dialog = Dialog(fragment)
            dialog?.let { dg ->
                dg.setCancelable(false)
                dg.setContentView(R.layout.fragment_dialog)
                dg.window?.setBackgroundDrawable(ColorDrawable(android.graphics.Color.TRANSPARENT))
                val tv: TextView = dg.findViewById(R.id.tv_message)
                tv.text = text
                val dialogMenu: Button = dg.findViewById(R.id.btn_menu)
                dialogMenu.setOnClickListener {
                    SoundsPlayer.getPlayer(fragmentContext, SoundsPlayer.BTN_SOUND)?.start()
                    dg.dismiss()
                    findNavController(view).navigate(menuAction)
                }
                val dialogRestart: Button = dg.findViewById(R.id.btn_restart)
                dialogRestart.setOnClickListener {
                    SoundsPlayer.getPlayer(fragmentContext, SoundsPlayer.BTN_SOUND)?.start()
                    dg.dismiss()
                    findNavController(view).navigate(restartAction)
                }
                dg.show()
            }
        }
    }
}
