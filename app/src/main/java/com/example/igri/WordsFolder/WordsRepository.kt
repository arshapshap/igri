package com.example.igri.WordsFolder

import com.example.igri.TroikaFolder.Trie

object WordsRepository {
    var words = Trie()
    fun fillRepository(wordsArray : Array<String>) {
        words = Trie()
        wordsArray.forEach {
            words.add(it)
        }
    }

    fun isWord(word:String):Boolean{
        return words.isWord(word)
    }

    fun isSubword(bigWord:String, smallWord:String):Boolean{
        for(letter in smallWord)
            if(smallWord.count { currentLetter: Char -> currentLetter == letter } >
                bigWord.count { currentLetter: Char -> currentLetter == letter })
                return false
        return true
    }

    fun getLongWord(n:Int):String = words.getLongWord(n)

    fun longWordsNumber() = words.longWordsNumber()
}

