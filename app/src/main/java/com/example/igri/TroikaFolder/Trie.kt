package com.example.igri.TroikaFolder


class Trie {
    class TrieNode(_depth: Int) {
        var isWord: Boolean = false
        var depth: Int = _depth
        var longWordsNumber: Int = 0
        var childrenNodes: MutableMap<Char, TrieNode> = mutableMapOf()
        fun add(word: String) {
            if (isLongWord(word))
                longWordsNumber+=1
            if (depth == word.length) {
                isWord = true
                return
            }

            if (!childrenNodes.containsKey(word[depth]))
                childrenNodes[word[depth]] = TrieNode(depth + 1)
            childrenNodes[word[depth]]!!.add(word)
        }

        private fun isLongWord(word: String): Boolean = word.length > 7


        fun contains(word: String): Boolean {
            if (depth == word.length) return isWord
            if (!childrenNodes.containsKey(word[depth])) return false
            return childrenNodes[word[depth]]!!.contains(word)
        }

        fun getLongWordByNumber(n: Int): String {
            if (n <= 0 || n > longWordsNumber) throw IndexOutOfBoundsException()
            var longWordsSkipped = 0
            for (nextLetter in childrenNodes.keys) {
                if (longWordsSkipped + childrenNodes[nextLetter]!!.longWordsNumber < n)
                    longWordsSkipped += childrenNodes[nextLetter]!!.longWordsNumber
                else return nextLetter + childrenNodes[nextLetter]!!.getLongWordByNumber(n-longWordsSkipped)
            }
            return ""
        }
    }

    var root: TrieNode = TrieNode(0)
    fun longWordsNumber():Int { return root.longWordsNumber}
    fun add(word: String) {
        root.add(word)
    }

    fun isWord(word: String): Boolean = root.contains(word)
    fun getLongWord(n:Int):String = root.getLongWordByNumber(n)
}